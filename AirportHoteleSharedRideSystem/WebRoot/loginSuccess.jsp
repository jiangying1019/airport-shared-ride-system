<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>User Login Success</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
   <script type="text/javascript" src="js/load.js"></script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">Airport Hotel Shared Ride System</a></h1>
         <!-- <h2>Simple. Contemporary. Website Template.</h2> --> 
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.jsp">Home</a></li>
          <li><a href="browse.jsp">Browse</a></li>
          <li><a href="search.jsp">Search</a></li>
          <li class="selected"><a href="loginSuccess.jsp">User</a></li>
           <li><a href="index.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="banner"></div>
	  <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
          <h3>Buses and Route Information</h3>
            <ul>
              <li><a href="listBusesUserPart.jsp">Bus Information</a></li>
              <li><a href="listRoutesUserPart.jsp">Route Information</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Order Information</h3>
            
            <ul>
            
              <li><a href="/AirportHotelSharedRideSystem/order!checkOrders.action?order.email=${user.email}">Orders</a></li>
              <li><a href="reserveSeat.jsp">Reserve a Seat</a></li>
              <li><a href="reviseOrder.jsp">Revise Orders</a></li>
              

            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
       <h1>Welcome to the Airport Hotel Shared Ride System</h1>
        <p> Airport Share Ride System is a ride matching system that allows commuters to quickly and securely find a commute match based on proximity, destination and travel route,  as well as schedules and preferences.It provides a cost-effective shared ride system between hotels and airports.  </p>
        <p>This system will be mainly used by the travelers who wish to reserve the service either in advance or when they just arrive/leave. </p>
        <p>Learn which commute option works best for you.Getting started is easy! </p>
         <h1>Welcome to the Airport Hotel Shared Ride System</h1>
        <p> Airport Share Ride System is a ride matching system that allows commuters to quickly and securely find a commute match based on proximity, destination and travel route,  as well as schedules and preferences.It provides a cost-effective shared ride system between hotels and airports.  </p>
        <p>This system will be mainly used by the travelers who wish to reserve the service either in advance or when they just arrive/leave. </p>
        <p>Learn which commute option works best for you.Getting started is easy! </p> 
       
      </div>
    </div>
    <div id="content_footer">
    
      
    <input type="hidden" name="user.email" value="${user.email}" />
    <s:set name="user" scope="application" value="user"/>
    </div>
    <div id="footer">
     <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="searchs.jsp">Search</a> | <a href="login">Login</a> | <a href="index.jsp">Logout</a></p>
      <p>Copyright &copy; Jiang Ying</p>
    </div>
  </div>
</body>
</html>