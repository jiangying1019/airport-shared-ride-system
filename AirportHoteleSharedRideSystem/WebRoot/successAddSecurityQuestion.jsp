<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
  <title>Add Security Questions</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
   <script type="text/javascript" src="js/load.js"></script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">Airport Hotel Shared Ride System</a></h1>
         </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.jsp">Home</a></li>
          <li><a href="browse.jsp">Browse</a></li>
          <li><a href="search.jsp">Search</a></li>
          <li class="selected"><a href="adminsLoginSuccess.jsp">Admin</a></li>
          <li><a href="index.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
    <div id="banner"></div>
      <div id="sidebar_container">
       <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Buses and Route Information</h3>
            <ul>
              <li><a href="listBusAdminPart.jsp">Bus Information</a></li>
              <li><a href="listRouteAdminPart.jsp">Route Information</a></li>
              <li><a href="listHotels.jsp">Hotel/Airport Information</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
   
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Operations</h3>
            <ul>
              <li><a href="addBuses.jsp">Add Buses</a></li>
              <li><a href="deleteBuses.jsp">Delete Buses</a></li>
              <li><a href="reassignBuses.jsp">Reassign Buses</a></li>
              <li><a href="addRoutes.jsp">Add Routes</a></li>
              <li><a href="deleteRoutes.jsp">Delete Routes</a></li>
              <li><a href="addHotels.jsp">Add Hotels/Airports</a></li>
              <li><a href="deleteHotels.jsp">Delete Hotel/Airports</a></li>
              <li><a href="addSecurityQuestions.jsp">Add Security Questions</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
     	  <!-- insert the page content here -->
        
          <div class="form_settings">
         <!-- Here is an Order form -->
      	<s:form method="post" action="securityQuestion!add.action">
        <h1>Add a Security Question</h1>
       	Success! A security question is added!
         <table>
				<tr>
					<td>Security Question</td>
					<td><input type="text" name="securityQuestion.name" id="question"/></td>
				</tr>

				<tr>
					<td style="width:30%; text-align: right"></td>
					<td><input type="submit" value="Submit" onclick="javasrcipt:return checkSecurityQuestion();"/></td>
				</tr>							
			</table>
      	</s:form>
          </div>             
       
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
     <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="search.jsp">Search</a> | <a href="login.jsp">Login</a> | <a href="index.jsp">Logout</a></p>
      <p>Copyright &copy; Jiang Ying</p>
      </div>
  </div>
</body>
</html>
