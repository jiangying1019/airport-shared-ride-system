<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Browse</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
   <script type="text/javascript" src="js/load.js"></script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1>Airport Hotel Shared Ride System</h1>
         
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.jsp">Home</a></li>
          <li class="selected"><a href="browse.jsp">Browse</a></li>
          <li><a href="search.jsp">Search</a></li>
          <li><a href="login.jsp">Login</a></li>
          <li><a href="login.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="banner"></div>
      <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
           <h3>Latest News</h3>
            <h4>New Website Launched</h4>
            <h5>October 21st, 2016</h5>
            <p>Latest News<br /></p>
            <p>Latest News<br /></p>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Useful Links</h3>
            <ul>
              <li><a href="#">fake link 1</a></li>
              <li><a href="#">fake link 2</a></li>
              <li><a href="#">fake link 3</a></li>
              <li><a href="#">fake link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <p>This page will contain the information of Airports/Hotels and buses.</p>
        <h1>Airports/Hotels Information</h1>
        <p>Airports/Hotels Information</p>
        <p>Airports/Hotels Information</p>
        <p>Airports/Hotels Information</p>
        <p>Airports/Hotels Information</p>
        
        <h1>Buses Information</h1>
        <p>Buses Information</p>
        <p>Buses Information</p>
        <p>Buses Information</p>
        <p>Buses Information</p>
        
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
     <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="search.jsp">Search</a> | <a href="login.jsp">Login</a> | <a href="index.html">Logout</a></p>
     <p>Copyright &copy; Jiang Ying</p>
    </div>
  </div>
</body>
</html>