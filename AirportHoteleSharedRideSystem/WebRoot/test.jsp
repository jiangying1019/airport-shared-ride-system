<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
 
<html>
<head>
<title>SELECT Operation</title>
</head>
<body>
 
<sql:setDataSource var="question" driver="com.mysql.jdbc.Driver"
     url="jdbc:mysql://localhost/ridesystem"
     user="root"  password="0121"/>
 
<sql:query dataSource="${question}" var="result">
SELECT * from securityquestion;
</sql:query>
 
<table border="1">
<tr>
   <th>Emp ID</th>
   <th>Security Question</th>
   
</tr>
<c:forEach var="row" items="${result.rows}">
<tr>
   <td><c:out value="${row.id}"/></td>
   <td><c:out value="${row.name}"/></td>
</tr>
</c:forEach>
</table>

  <select name="othStates" size="1" id="oth-states">
      <c:forEach items="${result.rows}" var="row">
            <option value="${row.id}"><c:out value="${row.name}"/></option>
    	</c:forEach>
   </select>

 
</body>
</html>