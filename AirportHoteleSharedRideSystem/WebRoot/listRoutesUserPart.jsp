<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*" import="java.text.*" %>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Reserve a Seat</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
  <script type="text/javascript" src="js/load.js"></script>
   
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
   <link rel="stylesheet" type="text/css" href="style/style.css" />
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">Airport Hotel Shared Ride System</a></h1>
          </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.jsp">Home</a></li>
          <li><a href="browse.jsp">Browse</a></li>
          <li><a href="search.jsp">Search</a></li>
          <li class="selected"><a href="loginSuccess.jsp">User</a></li>
          <li><a href="index.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
    <div id="banner"></div>
        <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Buses and Route Information</h3>
            <ul>
              <li><a href="listBusesUserPart.jsp">Bus Information</a></li>
              <li><a href="listRoutesUserPart.jsp">Route Information</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Order Information</h3>
            <ul>
              <li><a href="/AirportHotelSharedRideSystem/order!checkOrders.action?order.email=${user.email}">Orders</a></li>
              <li><a href="reserveSeat.jsp">Reserve a Seat</a></li>
              <li><a href="reviseOrder.jsp">Revise Orders</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        </div>
        
      <div id="content">
        <!-- insert the page content here -->
       
          <div class="form_settings">
         <!-- Here is an Order form -->
         
        <sql:setDataSource var="source" driver="com.mysql.jdbc.Driver"
   					  url="jdbc:mysql://localhost/ridesystem"
 					  user="root"  password="0121"/>
		
		<sql:query dataSource="${source}" var="routes">
			SELECT * from route;
		</sql:query>
		
       <h1>Route Information</h1>
		
		<table border="1">
		<tr>
 		  <th>Route Name</th>
 		  <th>Point one</th>
		  <th>Duration</th>
		  <th>Point two</th>
		  <th>Duration</th>
		  <th>Point three</th>
		  <th>Duration</th>
		  <th>Point four</th>
		  <th>Duration</th>
	   </tr>
	   
		<c:forEach var="row" items="${routes.rows}">
		<tr>
   		<td><c:out value="${row.name}"/></td>
   		<td><c:out value="${row.point1}"/></td>
   		<td><c:out value="${row.duration1}"/></td>
   		<td><c:out value="${row.point2}"/></td>
   		<td><c:out value="${row.duration2}"/></td>
   		<td><c:out value="${row.point3}"/></td>
   		<td><c:out value="${row.duration3}"/></td>
   		<td><c:out value="${row.point4}"/></td>
   		<td><c:out value="${row.duration4}"/></td>
		</tr>
		</c:forEach>
		</table>

          </div>       
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="search.jsp">Search</a> | <a href="login.jsp">Login</a> | <a href="index.jsp">Logout</a></p>
      <p>Copyright &copy; Jiang Ying</p>
    </div>
  </div>
</body>
</html>