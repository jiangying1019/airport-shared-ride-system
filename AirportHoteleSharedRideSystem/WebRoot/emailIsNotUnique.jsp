<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="s" uri="/struts-tags"%>
    <%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Register</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
   <script type="text/javascript" src="js/load.js"></script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.html">Airport Hotel Shared Ride System</a></h1>
         <!-- <h2>Simple. Contemporary. Website Template.</h2> --> 
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li ><a href="index.jsp">Home</a></li>
          <li><a href="browse.jsp">Browse</a></li>
          <li><a href="search.jsp">Search</a></li>
          <li class="selected"><a href="login.jsp">Login</a></li>
          <li><a href="register.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="banner"></div>
	  <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
          <h3>Latest News</h3>
            <h4>New Website Launched</h4>
            <h5>October 21st, 2016</h5>
            <p>Latest News<br /></p>
            <p>Latest News<br /></p>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Some Links</h3>
            <ul>
              <li><a href="#">fake link 1</a></li>
              <li><a href="#">fake link 2</a></li>
              <li><a href="#">fake link 3</a></li>
              <li><a href="#">fake link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
     <div id="content">
        <!-- insert the page content here -->
        
          <div class="form_settings">
     
      <s:form method="post" action="user!add.action">
          <h1>Register</h1>
   			Email is not unique!
         <table>
         		<tr>
					<td>Last Name:</td>
					<td><input type="text" name="user.lastname" id="lastname" /></td>
				</tr>

				<tr>
					<td >First Name:</td>
					<td><input type="text" name="user.firstname" id="firstname"/></td>
				</tr>
				
         		<tr>
					<td>Credit Card Number:</td>
					<td><input type="text" name="user.creditcard" id="creditcard"/></td>
				</tr>
				
				<tr>
					<td>Email:</td>
					<td><input type="text" name="user.email" id="email"/></td>
				</tr>
				
				<sql:setDataSource var="question" driver="com.mysql.jdbc.Driver"
    				 url="jdbc:mysql://localhost/ridesystem"
   					 user="root"  password="0121"/>
 				<sql:query dataSource="${question}" var="result">
					SELECT * from securityquestion;
				</sql:query>
				
				<tr>
				<td>Security Question</td>
				<td>
				 <select name="user.question1" size="1" id="question1">
     			 <c:forEach items="${result.rows}" var="row">
         	 	 <option value="${row.name}"><c:out value="${row.name}"/></option>
    			 </c:forEach>
  				 </select>
				</td>
				</tr>
				
				<tr>
					<td>Answer:</td>
					<td><input type="text" name="user.answer1" id="answer1"/></td>
				</tr>
				
				<tr>
				<td>Security Question</td>
				<td>
				 <select name="user.question2" size="1" id="question2">
     			 <c:forEach items="${result.rows}" var="row">
         	 	 <option value="${row.name}"><c:out value="${row.name}"/></option>
    			 </c:forEach>
  				 </select>
				</td>
				</tr>
				
				<tr>
					<td>Answer:</td>
					<td><input type="text" name="user.answer2" id="answer2"/></td>
				</tr>
				
				<tr>
				<td>Security Question</td>
				<td>
				 <select name="user.question3" size="1" id="question3">
     			 <c:forEach items="${result.rows}" var="row">
         	 	 <option value="${row.name}"><c:out value="${row.name}"/></option>
    			 </c:forEach>
  				 </select>
				</td>
				</tr>
				
				<tr>
					<td>Answer:</td>
					<td><input type="text" name="user.answer3" id="answer3"/></td>
				</tr>
				
				<tr>
					<td style="width:30%; text-align: right"></td>
					<td><input type="submit" value="Submit" onclick="javasrcipt:return checkRegister();"/></td>
				</tr>							
			</table>
      	</s:form>
          </div>       
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="search.jsp">Search</a> | <a href="login.jsp">Login</a> | <a href="index.jsp">Logout</a></p>
      <p>Copyright &copy; Jiang Ying</p>
    </div>
  </div>
</body>
</html>