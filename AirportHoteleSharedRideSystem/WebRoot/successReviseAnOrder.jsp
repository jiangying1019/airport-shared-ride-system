<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" import="java.util.*"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title>Revise Order</title>
  
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
   <script type="text/javascript" src="js/load.js"></script>
   
  <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
  <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
  <script src="http://code.jquery.com/ui/1.10.2/jquery-ui.js"></script>  
  
  <script>
  $(function() {
    $( ".datepicker" ).datepicker();
  });
  </script>
 
   
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">Airport Hotel Shared Ride System</a></h1>
          </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.jsp">Home</a></li>
          <li><a href="browse.jsp">Browse</a></li>
          <li><a href="search.jsp">Search</a></li>
          <li class="selected"><a href="loginSuccess.jsp">User</a></li>
          <li><a href="index.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="banner"></div>
        <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
            <h3>Buses and Route Information</h3>
            <ul>
              <li><a href="listBusesUserPart.jsp">Bus Information</a></li>
              <li><a href="listRoutesUserPart.jsp">Route Information</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Order Information</h3>
            <ul>
               <li><a href="/AirportHotelSharedRideSystem/order!checkOrders.action?order.email=${user.email}">Orders</a></li>
              <li><a href="reserveSeat.jsp">Reserve a Seat</a></li>
              <li><a href="reviseOrder.jsp">Revise Orders</a></li>
              
              
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        </div>
        
      <div id="content">
        <!-- insert the page content here -->
       
          <div class="form_settings">
         <!-- Here is an Order form -->
         
        <sql:setDataSource var="hotel" driver="com.mysql.jdbc.Driver"
   					  url="jdbc:mysql://localhost/ridesystem"
 					  user="root"  password="0121"/>
 		<sql:query dataSource="${hotel}" var="hotels">
			SELECT * from hotel;
		</sql:query>
		
		<sql:setDataSource var="bus" driver="com.mysql.jdbc.Driver"
   					  url="jdbc:mysql://localhost/ridesystem"
 					  user="root"  password="0121"/>
 					<sql:query dataSource="${bus}" var="buses">
						SELECT * from bus;
		</sql:query>
		
		<sql:setDataSource var="order" driver="com.mysql.jdbc.Driver"
   					  url="jdbc:mysql://localhost/ridesystem"
 					  user="root"  password="0121"/>
 					<sql:query dataSource="${order}" var="orders">
						SELECT orderid from orders where email=?;
						<sql:param value="${user.email}" />
					</sql:query>
     
      <s:form method="post" action="order!update.action">
           <h1>Revise an Order</h1>
           Success! The order is revised!
          <input type="hidden" name="order.email" value="${user.email }" />
         <table>
         	<tr>
         	<td>Order ID</td>
         	<td>
         	<select name="order.orderid" size="1" id="orderid">
    				 <c:forEach items="${orders.rows}" var="row">
          			  <option value="${row.orderid}"><c:out value="${row.orderid}"/></option>
    				</c:forEach>
				   </select>
         	</td>
         	</tr>
         
         		<tr>
					<td>Approach:</td>
					<td>
					<select name="order.approach" size="1" id="approach">
    				 <c:forEach items="${hotels.rows}" var="row">
          			  <option value="${row.name}"><c:out value="${row.name}"/></option>
    				</c:forEach>
				   </select>
					</td>
				</tr>

				<tr>
					<td>Destination</td>
					<td>
					<select name="order.destination" size="1" id="destination">
    				 <c:forEach items="${hotels.rows}" var="row">
          			  <option value="${row.name}"><c:out value="${row.name}"/></option>
    				</c:forEach>
				   </select>
					</td>
				</tr>

				<tr>
					<td >Bus Number:</td>
					<td>
					<select name="order.busnumber" size="1" id="busnumber">
    				 <c:forEach items="${buses.rows}" var="row">
          			  <option value="${row.busnumber}"><c:out value="${row.busnumber}"/></option>
    				</c:forEach>
				   </select>
					
					</td>
				</tr>

				<tr>
					<td>Date:</td>
					<td><input type="text" class="datepicker" name="order.date" id="date" onchange="checkDate()"/></td>
				</tr>
				
				<tr>
					<td >Departure Time:</td>
					<td>
					<select name="order.leavetime" id="departuretime" size="1">
					<option value="6:00">6:00AM</option>
					<option value="6:30">6:30AM</option>
					<option value="7:00">7:00AM</option>
					<option value="7:30">7:30AM</option>
					<option value="8:00">8:00AM</option>
					<option value="8:30">8:30AM</option>
					<option value="9:00">9:00AM</option>
					<option value="9:30">9:30AM</option>
					<option value="10:00">10:00AM</option>
					<option value="10:30">10:30AM</option>
					<option value="11:00">11:00AM</option>
					<option value="11:30">11:30AM</option>
					<option value="12:00">12:00PM</option>
					<option value="12:30">12:30PM</option>
					<option value="13:00">13:00PM</option>
					<option value="13:30">13:30PM</option>
					<option value="14:00">14:00PM</option>
					<option value="14:30">14:30PM</option>
					<option value="15:00">15:00PM</option>
					<option value="15:30">15:30PM</option>
					<option value="16:00">16:00PM</option>
					<option value="16:30">16:30PM</option>
					<option value="17:00">17:00PM</option>
					<option value="17:30">17:30PM</option>
					<option value="18:00">18:00PM</option>
					<option value="18:30">18:30PM</option>
					</select>
					</td>
				</tr>

				<tr>
					<td style="width:30%; text-align: right"></td>
					<td><input type="submit" value="Submit" onclick="javasrcipt:return changeOrder();"/></td>
				</tr>							
			</table>
      	</s:form>
          </div>       
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
     <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="search.jsp">Search</a> | <a href="login.jsp">Login</a> | <a href="index.jsp">Logout</a></p>
      <p>Copyright &copy; Jiang Ying</p>
    </div>
  </div>
</body>
</html>
