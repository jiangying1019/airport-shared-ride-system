	
/*******************For addBuses.jsp, addBusSuccess.jsp************************/
	 function checkBus(){
		 if(document.getElementById("busnumber").value==""){
			 alert("Bus Number is empty!");
			 return false;
		 }else if(document.getElementById("route").value==""){
			 alert("Route is empty!");
			 return false;
		 }else if(document.getElementById("starttime").value==""){
			 alert("Start Time is empty!");
		 }else{
			 return true;
		 }
	 }
	 
/********************For addHotels.jsp***************************/
	 function checkHotel(){
		 if(document.getElementById("hotelname").value==""){
			 alert("Hotel/Airport name is empty!");
			 return false;
		 }else{
			 return true;
		 }
	 }
	 
/****************************addRoutes.jsp**************************/
	 function checkRoute(){
		
		 var point1 = document.getElementById("point1").selectedIndex;
		 var point2 = document.getElementById("point2").selectedIndex;
		 var point3 = document.getElementById("point3").selectedIndex;
		 var point4 = document.getElementById("point4").selectedIndex;
		 var v1 = document.getElementsByTagName("option")[point1].value;
		 var v2 = document.getElementsByTagName("option")[point2].value;
		 var v3 = document.getElementsByTagName("option")[point3].value;
		 var v4 = document.getElementsByTagName("option")[point4].value;
		
		 if(document.getElementById("routename").value==""){
			 alert("Route name is empty!");
			 return false;
		 }else if(document.getElementById("point1").value==""){
			 alert("Point 1 is empty!");
			 return false;
		 }else if(document.getElementById("duration1").value==""){
			 alert("Duration 1 is empty!");
			 return false;
		 }else if(document.getElementById("point2").value==""){
			 alert("Point 2 is empty!");
			 return false;
		 }else if(document.getElementById("duration2").value==""){
			 alert("Duration 2 is empty!");
			 return false;
		 }else if(document.getElementById("point3").value==""){
			 alert("Point 3 is empty!");
			 return false;
		 }else if(document.getElementById("duration3").value==""){
			 alert("Duration 3 is empty!");
			 return false;
		 }else if(document.getElementById("point4").value==""){
			 alert("Point 4 is empty!");
			 return false;
		 }else if(document.getElementById("duration4").value==""){
			 alert("Duration 4 is empty!");
			 return false;
		 }else  if(v1==v2){
			 alert("Point1 and point2 are the same!");
			 return false;
		 }else  if(v1==v3){
			 alert("Point1 and point3 are the same!");
			 return false;
		 }else if(v1==v4){
			 alert("Point1 and point4 are the same!");
			 return false;
		 }else  if(v2==v3){
			 alert("Point2 and point3 are the same!");
			 return false;
		 }else if(v2==v4){
			 alert("Point2 and point4 are the same!");
			 return false;
		 }else  if(v3==v4){
			 alert("Point3 and point4 are the same!");
			 return false;
		 }else {
			 return true;
		 }
	 }
	 
/***********************admin.jsp***********************************/
	 function checkAdmin(){
		 if(document.getElementById("name").value==""){
			 alert("Email is empty!");
			 return false;
		 }else if(document.getElementById("password").value==""){
			 alert("Password is empty!");
			 return false;
		 }else{
			 return true;
		 }
	 }
	 
	 /********************changeBusRoute********************************/
	 function changeBusRoute(){
		 if(document.getElementById("busnumber").value==""){
			 alert("Bus number is empty!");
			 return false;
		 }else if(document.getElementById("route").value==""){
			 alert("Route is empty!");
			 return false;
		 }else{
			 return true;
		 }
	 }

/******************************deleteBus.jsp*****************************/
	 function deleteBus(){
		 if(document.getElementById("busnumber").value==""){
			 alert("Bus number is empty!");
			 return false;
		 }else {
			 return true;
		 }
	 }
	 
/***********************deleteHotle.jsp*****************************/
	 function deleteHotel(){
		 if(document.getElementById("hotelname").value==""){
			 alert("Hotel name is empty!");
			 return false;
		 }else{
			 return true;
		 }
	 }
/**************************deleteRoute.jsp*********************************/
	 function deleteRoute(){
		 if(document.getElementById("routename").value==""){
			 alert("Route name is empty!");
			 return false;
		 }else{
			 return true;
		 }
	 }
/********************login.jsp*****************/
	 function userLogin(){
		 if(document.getElementById("email").value==""){
			 alert("Email is empty!");
			 return false;
		 }else if(document.getElementById("password").value==""){
			 alert("Password is empty!");
			 return false;
		 }else if(document.getElementById("answer2").value==""){
			 alert("Security question is empty!");
			 return false;
		 }else{
			 return true;
		 }
	 }
/*******************order part*************************/
	 function changeOrder(){
		 var appr = document.getElementById("approach").selectedIndex;
		 var dest = document.getElementById("destination").selectedIndex;
		 var v1 = document.getElementsByTagName("option")[appr].value;
		 var v2 = document.getElementsByTagName("option")[dest].value;
		 
		 if(document.getElementById("orderid").value==""){
			 alert("Order id is empty!");
			 return false;
		 }else if(document.getElementById("approach").value==""){
			 alert("Approach is empty!");
			 return false;
		 }else if(document.getElementById("destination").value==""){
			 alert("Destination is empty!");
			 return false;
		 }else if(document.getElementById("busnumber").value==""){
			 alert("Bus number is empty!");
			 return false;
		 }else if(document.getElementById("date").value==""){
			 alert("Date is empty!");
			 return false;
		 }else if(document.getElementById("departuretime").value==""){
			 alert("Departure time is empty!");
			 return false;
		 }else if(v1==v2){
			 alert("Approach and destination are the same!");
			 return false;
		 }else if(!checkDate()){
			 alert("Date must be in the future!");
			 return false;
		 }
		 else{
			 return true;
		 }
	 }

	 
	 
/**********************reserve Seat******************************/
	 function reserveSeat(){
		 var appr = document.getElementById("approach").selectedIndex;
		 var dest = document.getElementById("destination").selectedIndex;
		 var v1 = document.getElementsByTagName("option")[appr].value;
		 var v2 = document.getElementsByTagName("option")[dest].value;
		 
		 if(document.getElementById("approach").value==""){
			 alert("Approach is empty!");
			 return false;
		 }else if(document.getElementById("destination").value==""){
			 alert("Destination is empty!");
			 return false;
		 }else if(document.getElementById("busnumber").value==""){
			 alert("Bus number is empty!");
			 return false;
		 }else if(document.getElementById("date").value==""){
			 alert("Date is empty!");
			 return false;
		 }else if(document.getElementById("departuretime".value=="")){
			 alert("Departure time is empty!");
			 return false;
		 }else if(v1==v2){
			 alert("Approach and destination are the same!");
			 return false;
		 }else if(!checkDate()){
			 alert("Date must be in the future!");
			 return false;
		 }
		 else{
			 return true;
		 }
	 }
	 
	 function checkDate(){
		 var date = document.getElementById("date").value;
		 var now = new Date();
		 var dt1 = Date.parse(now);
		 var dt2 = Date.parse(date);
		 if(dt2<dt1){
			//alert("Date must be in the future!");
			 return false;
		 }else
			 return true;
	 }
	 /*******************register part ***********************/
	 function checkRegister(){
		 var q1 = document.getElementById("question1").selectedIndex;
		 var q2 = document.getElementById("question2").selectedIndex;
		 var q3 = document.getElementById("question3").selectedIndex;
		 var v1 = document.getElementsByTagName("option")[q1].value;
		 var v2 = document.getElementsByTagName("option")[q2].value;
		 var v3 = document.getElementsByTagName("option")[q3].value;
		 
		 if(document.getElementById("lastname").value==""){
			 alert("Last Name is empty!");
			 return false;
		 }else if(document.getElementById("firstname").value==""){
			 alert("First Name is empty!");
			 return false;
		 }else if(document.getElementById("creditcard").value==""){
			 alert("Credit card is empty!");
			 return false;
		 }else if(document.getElementById("creditcard").value.length!=16){
			 alert("Credit card number is invalid!");
			 return false;
		 }else if(document.getElementById("email").value==""){
			 alert("Emial is empty!");
			 return false;
		 }else if(document.getElementById("answer1").value==""){
			 alert("Answer one is empty!");
			 return false;
		 }else if(document.getElementById("answer2").value==""){
			 alert("Answer two is empty!");
			 return false;
		 }else if(document.getElementById("answer3").value==""){
			 alert("Answer three is empty");
			 return false;
		 }else if(v1==v2){
			 alert("Security question one and two are the same!");
			 return false;
		 }else if(v1==v3){
			 alert("Security question one and three are the same!");
			 return false;
		 }else if(v2==v3){
			 alert("Security question two and three are the same!");
			 return false;
		 }else{
			 return true;
		 }
	 }
	 
/*********************search availability****************************/
	 function checkAvailability(){
		 var p1 = document.getElementById("point1").selectedIndex;
		 var p2 = document.getElementById("point2").selectedIndex;
		 var v1 = document.getElementsByTagName("option")[p1].value;
		 var v2 = document.getElementsByTagName("option")[p2].value;
		 
		 if(v1 == v2){
			 alert("The approach and destination are the same!");
			 return false;
		 }else{
			 return true;
		 }
	 }
	 
/***********check security question*******************/	 
	 function checkSecurityQuestion(){
		 if(document.getElementById("question").value==""){
			 alert("Security question is empty!");
			 return false;
		 }else 
			 return true;
	 }
	 
	 
	 
	 