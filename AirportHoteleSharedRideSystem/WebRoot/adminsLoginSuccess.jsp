<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
  <title>Admins Login Success</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
   <script type="text/javascript" src="js/load.js"></script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1><a href="index.jsp">Airport Hotel Shared Ride System</a></h1>
         </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.jsp">Home</a></li>
          <li><a href="browse.jsp">Browse</a></li>
          <li><a href="search.jsp">Search</a></li>
          <li class="selected"><a href="adminsLoginSuccess.jsp">Admin</a></li>
          <li><a href="index.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
      <div id="sidebar_container">
       <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
           <h3>Buses and Route Information</h3>
            <ul>
              <li><a href="listBusAdminPart.jsp">Bus Information</a></li>
              <li><a href="listRouteAdminPart.jsp">Route Information</a></li>
              <li><a href="listHotels.jsp">Hotel/Airport Information</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
   
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Operations</h3>
            <ul>
              <li><a href="addBuses.jsp">Add Buses</a></li>
              <li><a href="deleteBuses.jsp">Delete Buses</a></li>
              <li><a href="reassignBuses.jsp">Reassign Buses</a></li>
              <li><a href="addRoutes.jsp">Add Routes</a></li>
              <li><a href="deleteRoutes.jsp">Delete Routes</a></li>
              <li><a href="addHotels.jsp">Add Hotels/Airports</a></li>
              <li><a href="deleteHotels.jsp">Delete Hotels/Airports</a></li>
              <li><a href="addSecurityQuestions.jsp">Add Security Questions</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <h1>Admins Page</h1>
        <p>There will be administrators of the system who will manage the details regarding hotels, air ports, number of services available, their schedules, fees and so on.</p>
        <p>•Add bus –Every bus must be given a unique bus number when adding. It should also be assigned to a route and given a timetable even if there is another bus in the same route.</p>
		<p>•Delete bus –The input is the bus number. The bus will be removed from the database. If this is the only bus in its route, then the corresponding route will also be deleted. </p>
	    <p>•Add route -The input is a unique route name, the pick-up/drop-off hotels/airports in that route. A bus must be assigned to this route.</p>
		<p>•Delete route -The input is the route name. Any bus assigned to the route will be re-assigned to another route.</p> 
		<p>•Re-assign bus –A bus which was originally assigned to one route will be assigned to a new route using this functionality.</p>
		<p>•Add hotels/airports -The input is unique hotel/airport name. When a hotel/airport is added to the system, a bus route must cover the hotel/airport. <p>
		<p>•Delete hotel/airport -The input is the unique hotel/airport name. If no bus route covers the hotel/airport, then it should be deleted from both system and database.</p>
		<p>•Add security question -Since every user needs to select at least three security questions</p>
       
       
      </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
      <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="search.jsp">Search</a> | <a href="login.jsp">Login</a> | <a href="index.jsp">Logout</a></p>
      <p>Copyright &copy; Jiang Ying</p>
    </div>
  </div>
</body>
</html>
