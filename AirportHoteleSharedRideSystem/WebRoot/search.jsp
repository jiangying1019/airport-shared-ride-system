<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ page import="java.io.*,java.util.*,java.sql.*"%>
<%@ page import="javax.servlet.http.*,javax.servlet.*" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql"%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
  <title>Search</title>
  <meta name="description" content="website description" />
  <meta name="keywords" content="website keywords, website keywords" />
  <meta http-equiv="content-type" content="text/html; charset=windows-1252" />
  <link rel="stylesheet" type="text/css" href="style/style.css" />
   <script type="text/javascript" src="js/load.js"></script>
</head>

<body>
  <div id="main">
    <div id="header">
      <div id="logo">
        <div id="logo_text">
          <!-- class="logo_colour", allows you to change the colour of the text -->
          <h1>Airport Hotel Shared Ride System</h1>
        </div>
      </div>
      <div id="menubar">
        <ul id="menu">
          <!-- put class="selected" in the li tag for the selected page - to highlight which page you're on -->
          <li><a href="index.jsp">Home</a></li>
          <li><a href="browse.jsp">Browse</a></li>
          <li class="selected"><a href="search.jsp">Search</a></li>
          <li><a href="login.jsp">Login</a></li>
          <li><a href="index.jsp">Logout</a></li>
        </ul>
      </div>
    </div>
    <div id="content_header"></div>
    <div id="site_content">
    <div id="banner"></div>
      <div id="sidebar_container">
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <!-- insert your sidebar items here -->
           <h3>Some Links</h3>
            <ul>
              <li><a href="#">fake link 1</a></li>
              <li><a href="#">fake link 2</a></li>
              <li><a href="#">fake link 3</a></li>
              <li><a href="#">fake link 4</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Buses and Route Information</h3>
            <ul>
              <li><a href="listBuses.jsp">Bus Information</a></li>
              <li><a href="listRoutes.jsp">Route Information</a></li>
            </ul>
          </div>
          <div class="sidebar_base"></div>
        </div>
        <div class="sidebar">
          <div class="sidebar_top"></div>
          <div class="sidebar_item">
            <h3>Search</h3>
            <form method="post" action="#" id="search_form">
              <p>
                <input class="search" type="text" name="search_field" value="Enter keywords....." />
                <input name="search" type="image" style="border: 0; margin: 0 0 -9px 5px;" src="style/search.png" alt="Search" title="Search" />
              </p>
            </form>
          </div>
          <div class="sidebar_base"></div>
        </div>
      </div>
      <div id="content">
        <!-- insert the page content here -->
        <p>This page will help users to search available buses.</p>
        
         <sql:setDataSource var="source" driver="com.mysql.jdbc.Driver"
   					  url="jdbc:mysql://localhost/ridesystem"
 					  user="root"  password="0121"/>
 		<sql:query dataSource="${source}" var="hotels">
			SELECT * from hotel;
		</sql:query>
		
       <h1>Search Buses</h1>
        <s:form method="post" action="route!searchAvailableBus.action">
        <table>
        <tr>
        <td>Pick me at:</td>
        <td>
        <select name="route.point1" size="1" id="point1">
    				 <c:forEach items="${hotels.rows}" var="row">
          			  <option value="${row.name}"><c:out value="${row.name}"/></option>
    				</c:forEach>
		</select>
        </td>
        </tr>
        
        <tr>
        <td>Drop me off:</td>
        <td>
         <select name="route.point2" size="1" id="point2">
    				 <c:forEach items="${hotels.rows}" var="row">
          			  <option value="${row.name}"><c:out value="${row.name}"/></option>
    				</c:forEach>
		</select>
        </td>
        </tr>
        
        <tr>
        <td></td>
        <td><input type="submit" value="Check Availability" onclick="javasrcipt:return checkAvailability();"/></td>
        </tr>
        </table>
      	</s:form>
		
     </div>
    </div>
    <div id="content_footer"></div>
    <div id="footer">
     <p><a href="index.jsp">Home</a> | <a href="browse.jsp">Browse</a> | <a href="search.jsp">Search</a> | <a href="login.jsp">Login</a> | <a href="index.jsp">Logout</a></p>
      <p>Copyright &copy; Jiang Ying</p>
    </div>
  </div>
</body>
</html>
