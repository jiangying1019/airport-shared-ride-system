package com.project.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.project.common.dao.BasicSupportDao;
import com.project.dao.interfaces.ISecurityQuestionDao;
import com.project.entities.SecurityQuestion;

@SuppressWarnings("serial")
@Repository("securityQuestionDao")
public class SecurityQuestionDao extends BasicSupportDao implements ISecurityQuestionDao{

	@Override
	public void add(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(securityQuestion);
		
	}

	@Override
	public List<SecurityQuestion> list(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
		return (List<SecurityQuestion>)this.getHibernateTemplate().find("from securityquestion");
	}

	@Override
	public SecurityQuestion detail(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
		String hql = "delete sq from SecurityQuestion where sq.name = '"+securityQuestion.getName()+"'";
		this.getHibernateTemplate().find(hql);
	}

}
