package com.project.dao.impl;

import java.util.List;
import java.util.Random;

import javax.mail.MessagingException;

import org.springframework.stereotype.Repository;

import com.project.common.dao.BasicSupportDao;
import com.project.dao.interfaces.IUserDao;
import com.project.entities.User;

@SuppressWarnings("serial")
@Repository("userDao")
public class UserDao extends BasicSupportDao implements IUserDao{

	@Override
	public void add(User user) {
		Random rand = new Random();
		int passwd = rand.nextInt(8999)+1000;
		user.setPassword(passwd+"");
		this.getHibernateTemplate().save(user);
		Mail mail = new Mail();
		try {
			String content = "Register Success! Your username is <b>"+user.getEmail()+"</b> and your password is <b>"+user.getPassword()+"</b>.";
			mail.sendAccount(user.getEmail(),"Username and Password",content);
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<User> list(User user) {
		return (List<User>)this.getHibernateTemplate().find("from User");
	}

	@Override
	public void update(User user) {
		this.getHibernateTemplate().update(user);
	}

	@Override
	public User detail(User user) {
		return (User)this.getHibernateTemplate().get(User.class, user.getUid());
	}

	@Override
	public void delete(User user) {
		this.getHibernateTemplate().delete(user);
	}
	
	public boolean checkEmailIsUnique(User user){
		String hql = "select user from User user where user.email='"+user.getEmail()+"'";
		if(this.getHibernateTemplate().find(hql).size()>0){
			return false;
		}else 
			return true;
	}

	@Override
	public User checkin(User user) {
		String sql = "select u from User u where u.email='" + user.getEmail()
				+ "'and u.password='"+user.getPassword()+"'and u.answer2='"+user.getAnswer2()+"'";
		List list = this.getHibernateTemplate().find(sql);
		if (list.isEmpty()) {
			User tempuser = new User();
			tempuser.setUid("");
			return tempuser;
		} else
			return (User) list.get(0);
	}
	
	public User searchForSecurityQuestion(User user){
		String hql = "select user from User user where user.email='"+user.getEmail()+"'";
		if(this.getHibernateTemplate().find(hql).size()>0)
		return (User)this.getHibernateTemplate().find(hql).get(0);
		else 
			return null;
	}
	
}