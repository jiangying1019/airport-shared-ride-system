package com.project.dao.impl;

import java.util.List;

import javax.mail.MessagingException;

import org.springframework.stereotype.Repository;

import com.project.common.dao.BasicSupportDao;
import com.project.dao.interfaces.IBusDao;
import com.project.entities.Bus;
import com.project.entities.Order;
import com.project.entities.Route;

@SuppressWarnings("serial")
@Repository("busDao")
public class BusDao extends BasicSupportDao implements IBusDao{

	@Override
	public void add(Bus bus) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(bus);
	}

	@Override
	public void delete(Bus bus) {
		// TODO Auto-generated method stub
		String hql = "select b from Bus b where b.busnumber = '"+bus.getBusnumber()+"'";
		bus = (Bus)this.getHibernateTemplate().find(hql).get(0);
		
		//if the bus is the only bus in that route, it should be deleted
		String routename = bus.getRoute();
		String findBusesOnRoute = "select b from Bus b where b.route='"+routename+"'";
		List<Bus> listbus = this.getHibernateTemplate().find(findBusesOnRoute);
		if(listbus.size()<=1){
		String deleteRoute = "select route from Route route where route.name='"+routename+"'";
		Route route = (Route) this.getHibernateTemplate().find(deleteRoute).get(0);
		this.getHibernateTemplate().delete(route);
		}
		
		//email part
		String hql1 = "select order from Order order where order.busnumber='"+bus.getBusnumber()+"'";
		List<Order> orders = this.getHibernateTemplate().find(hql1);
		String busnumber = bus.getBusnumber();
		this.getHibernateTemplate().delete(bus);
		//send mail part
		String email;
		String subject = "The bus you ordered is deleted";
		String content = "Dear passenger, bus "+busnumber+" is deleted! Please check you order and reschedule!";
		Mail mail = new Mail();
		for(int i=0;i<orders.size();i++){
			email = orders.get(i).getEmail();
			try {
				mail.sendAccount(email, subject, content);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<Bus> list(Bus bus) {
		// TODO Auto-generated method stub
		return (List<Bus>)this.getHibernateTemplate().find("from Bus");
	}

	@Override
	public boolean changeRoute(Bus bus) {
		// TODO Auto-generated method stub
		Bus tempBus = new Bus();
		tempBus.setRoute(bus.getRoute());
		String hql = "select b from Bus b where b.busnumber='"+bus.getBusnumber()+"'";
		bus = (Bus)this.getHibernateTemplate().find(hql).get(0);
		//send mail find the email
		String hql1 = "select order from Order order where order.busnumber='"+bus.getBusnumber()+"'";
		List<Order> orders = this.getHibernateTemplate().find(hql1);
		
		String email;
		String subject = "Route is changed";
		String content = "Dear passenger, bus "+bus.getBusnumber()+" in route "+bus.getRoute()+" has been change to "+tempBus.getRoute()+"! Please check you order and reschedule!";
		Mail mail = new Mail();
		
		if(!bus.getRoute().equals(tempBus.getRoute())){
			bus.setRoute(tempBus.getRoute());
			this.getHibernateTemplate().update(bus);
			//send mail part
			for(int i=0;i<orders.size();i++){
				email = orders.get(i).getEmail();
				try {
					mail.sendAccount(email, subject, content);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			return true;
		}
		return false;
	}
	
	public Bus detail(Bus bus) {
		// TODO Auto-generated method stub
		return (Bus)this.getHibernateTemplate().get(Bus.class, bus.getBusnumber());
	}

	@Override
	public List<Bus> listName(Bus bus) {
		// TODO Auto-generated method stub
		String hql = "select b from Bus b where b.busnumber='"+bus.getBusnumber()+"'";
		return (List<Bus>)this.getHibernateTemplate().find(hql);
	}

	@Override
	public List<Bus> listBusesNeedToBeChanged(Bus bus) {
		// TODO Auto-generated method stub
		String hql = "from Bus bus where bus.route not in ( select r.name from Route r )";
		return this.getHibernateTemplate().find(hql);
	}//no Route entity in this class

	@Override
	public void deleteRouteThenChangeRoute(Bus bus) {
		// TODO Auto-generated method stub
		Bus tempBus = new Bus();
		tempBus.setRoute(bus.getRoute());
		String hql = "select bus from Bus bus where bus.busid = '"+bus.getBusid()+"'";
		bus = (Bus)this.getHibernateTemplate().find(hql).get(0);
		
		String newRoute = tempBus.getRoute();
		String oldRoute = bus.getRoute();
		
		bus.setRoute(tempBus.getRoute());
		this.getHibernateTemplate().update(bus);
		//send mail to user part
		String hql1 = "select order from Order order where order.busnumber='"+bus.getBusnumber()+"'";
		List<Order> orders = this.getHibernateTemplate().find(hql1);
		
		String email;
		String subject = "The route is deleted";
		String content = "Dear passenger, bus "+bus.getBusnumber()+" in route "+oldRoute+" has been change to "+newRoute+"! Please check you order and reschedule!";
		Mail mail = new Mail();
		
		for(int i=0;i<orders.size();i++){
			email = orders.get(i).getEmail();
			try {
				mail.sendAccount(email, subject, content);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
