package com.project.dao.impl;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.mail.MessagingException;

import org.springframework.stereotype.Repository;

import com.project.common.dao.BasicSupportDao;
import com.project.dao.interfaces.IOrderDao;
import com.project.entities.Bus;
import com.project.entities.Order;
import com.project.entities.Route;

@SuppressWarnings("serial")
@Repository("orderDao")
public class OrderDao extends BasicSupportDao implements IOrderDao{

	@Override
	public boolean add(Order order) {
		// TODO Auto-generated method stub
			if(checkBusCoversPoints(order)){
				this.getHibernateTemplate().save(order);
				Mail mail = new Mail();
				String subject = "Order information";
				String content = "Dear passerger, you successfully reserved a seat! There is the information about the order: Approach: "+order.getApproach()
						+"Destination: "+order.getDestination()+"bus number"+order.getBusnumber()+"date: "+order.getDate()+"time: "+order.getLeavetime();
				try {
					mail.sendAccount(order.getEmail(), subject, content);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return true;
			}else{
				return false;
			}
	}

	@Override
	public List<Order> list(Order order) {
		// TODO Auto-generated method stub
		return (List<Order>)this.getHibernateTemplate().find("from Order");
	}

	@Override
	public int update(Order order) {
		// TODO Auto-generated method stub
		
		//String hql1 = "select o from Order o where o.orderid='"+order.getOrderid()+"' and o.email = '"+order.getEmail()+"'";
		if(!checkBusCoversPoints(order)){
			return 0;
		}else{
			Order tempOrder = new Order();
			tempOrder.setApproach(order.getApproach());
			tempOrder.setBusnumber(order.getBusnumber());
			tempOrder.setDestination(order.getDestination());
			tempOrder.setLeavetime(order.getLeavetime());
			tempOrder.setDate(order.getDate());
			String hql = "select o from Order o where o.orderid = '"+order.getOrderid()+"'";
			order = (Order)this.getHibernateTemplate().find(hql).get(0);
			order.setApproach(tempOrder.getApproach());
			order.setDestination(tempOrder.getDestination());
			order.setBusnumber(tempOrder.getBusnumber());
			order.setDate(tempOrder.getDate());
			order.setLeavetime(tempOrder.getLeavetime());
			this.getHibernateTemplate().update(order);
			//send mail part
			Mail mail = new Mail();
			String subject = "Revised Order information";
			String content = "Dear passerger, you successfully changed your order! There is the information about the new order: Approach: "+order.getApproach()
					+"Destination: "+order.getDestination()+"bus number"+order.getBusnumber()+"date: "+order.getDate()+"time: "+order.getLeavetime();
			try {
				mail.sendAccount(order.getEmail(), subject, content);
			} catch (MessagingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return 1;
		}
		
	}

	@Override
	public Order detail(Order order) {
		// TODO Auto-generated method stub
		return (Order)this.getHibernateTemplate().get(Order.class, order.getOrderid());
	}

	@Override
	public List<Order> checkOrders(Order order) {
		// TODO Auto-generated method stub
		String hql = "select o from Order o where o.email='"+order.getEmail()+"'";
		List<Order> list = this.getHibernateTemplate().find(hql);
		if(list.size()>0){
			order = (Order)list.get(0);
			return (List<Order>)this.getHibernateTemplate().find(hql);
		}
		return null;
	}
	
	public boolean checkBusCoversPoints(Order order){
		String hql = "from Route";
		List<Route> listRoute = (List<Route>)this.getHibernateTemplate().find(hql);
		Bus bus;
		String appr = order.getApproach();
		String dest = order.getDestination();
		String routename="";
		String busnumber = order.getBusnumber();
		
		for(int i=0;i<listRoute.size();i++){
			Set<String> set = new HashSet<String>();
			set.add(listRoute.get(i).getPoint1());
			set.add(listRoute.get(i).getPoint2());
			set.add(listRoute.get(i).getPoint3());
			set.add(listRoute.get(i).getPoint4());
			
			if(set.contains(appr)&&set.contains(dest)){
				routename = listRoute.get(i).getName();
				
				String hql1 = "select bus from Bus bus where bus.busnumber='"+busnumber+"'";
				 bus = (Bus) this.getHibernateTemplate().find(hql1).get(0);
				if(bus.getRoute().equals(routename)){
				return true;
				}
			}
		}
		return false;
	}
}
