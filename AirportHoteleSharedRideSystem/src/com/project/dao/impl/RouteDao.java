package com.project.dao.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Repository;

import com.project.common.dao.BasicSupportDao;
import com.project.dao.interfaces.IRouteDao;
import com.project.entities.Hotel;
import com.project.entities.Route;

@SuppressWarnings("serial")
@Repository("routeDao")
public class RouteDao extends BasicSupportDao implements IRouteDao{
	private Hotel hotel;

	public Hotel getHotel() {
		return hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	@Override
	public void add(Route route) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(route);
	}

	@Override
	public void delete(Route route) {
		// TODO Auto-generated method stub
		String hql = "select r from Route r where r.name = '"+route.getName()+"'";
		route = (Route)this.getHibernateTemplate().find(hql).get(0);
		this.getHibernateTemplate().delete(route);
	}

	@Override
	public Route detail(Route route) {
		// TODO Auto-generated method stub
		return (Route)this.getHibernateTemplate().get(Route.class, route.getName());
	}

	@Override
	public List<Route> list(Route route) {
		// TODO Auto-generated method stub
		return (List<Route>)this.getHibernateTemplate().find("from Route");
	}

	@Override
	public List<Route> listName(Route route) {
		// TODO Auto-generated method stub
		String hql = "select r from Route r where r.name = '"+route.getName()+"'";
		return (List<Route>)this.getHibernateTemplate().find(hql);
	}

	@Override
	public int checkPoint(Route route) {
		// TODO Auto-generated method stub
		String hql = "from Route";
		String point1 = route.getPoint1();
		String point2 = route.getPoint2();
		String point3 = route.getPoint3();
		String point4 = route.getPoint4();
		List<Route> listRoute = this.getHibernateTemplate().find(hql);
		
		for(int i=0;i<listRoute.size();i++){
			Set<String> set = new HashSet<String>();
			set.add(listRoute.get(i).getPoint1());
			set.add(listRoute.get(i).getPoint2());
			set.add(listRoute.get(i).getPoint3());
			set.add(listRoute.get(i).getPoint4());
			if(set.contains(point1)&&set.contains(point2)&&set.contains(point3)&&set.contains(point4)){
				return 0;
			}	
		}
		return 1;
	}

	@Override
	public int checkRouteExists(Route route) {
		// TODO Auto-generated method stub
		String hql = "select r from Route r where r.name='"+route.getName()+"'";
		return this.getHibernateTemplate().find(hql).size();
	}

	@Override
	public List<Route> searchAvailableBus(Route route) {
		// TODO Auto-generated method stub
		String point1 = route.getPoint1();
		String point2 = route.getPoint2();
		List<Route> listRoute = new ArrayList<Route>();
		
		String hql = "from Route";
		List<Route> list = this.getHibernateTemplate().find(hql);
		for(int i=0;i<list.size();i++){
			Set<String> set = new HashSet<String>();
			set.add(list.get(i).getPoint1());
			set.add(list.get(i).getPoint2());
			set.add(list.get(i).getPoint3());
			set.add(list.get(i).getPoint4());
			if(set.contains(point1)&&set.contains(point2)){
				listRoute.add(list.get(i));
			}
		}
		return listRoute;
	}
	
	

}
