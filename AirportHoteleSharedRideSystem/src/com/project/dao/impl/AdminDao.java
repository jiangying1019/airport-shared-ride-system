package com.project.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.project.common.dao.BasicSupportDao;
import com.project.dao.interfaces.IAdminDao;
import com.project.entities.Admin;

@SuppressWarnings("serial")
@Repository("adminDao")
public class AdminDao extends BasicSupportDao implements IAdminDao{

	@Override
	public List<Admin> list(Admin admin) {
		// TODO Auto-generated method stub
		return (List<Admin>)this.getHibernateTemplate().find("from admin");
	}

	@Override
	public void update(Admin admin) {
		this.getHibernateTemplate().update(admin);
	}

	@Override
	public Admin detail(Admin admin) {
		
		return (Admin)this.getHibernateTemplate().get(Admin.class, admin.getAid());
	}

	@Override
	public Admin checkin(Admin admin) {
		String sql = "select a from Admin a where a.aname='"+admin.getAname()+"'and a.passwd='"+admin.getPasswd()+"'";
		List list = this.getHibernateTemplate().find(sql);
		if(list.isEmpty()){
			Admin newAdmin = new Admin();
			newAdmin.setAid("");
			return newAdmin;
		}else
		return (Admin)list.get(0);
	}
}
