package com.project.dao.impl;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.project.common.dao.BasicSupportDao;
import com.project.dao.interfaces.IHotelDao;
import com.project.entities.Hotel;
import com.project.entities.Route;

@SuppressWarnings("serial")
@Repository("hotelDao")
public class HotelDao extends BasicSupportDao implements IHotelDao	{

	private Route route;
	
	public Route getRoute() {
		return route;
	}

	public void setRoute(Route route) {
		this.route = route;
	}

	@Override
	public void add(Hotel hotel) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().save(hotel);
	}

	@Override
	public List<Hotel> list(Hotel hotel) {
		// TODO Auto-generated method stub
		return (List<Hotel>)this.getHibernateTemplate().find("from Hotel");
	}

	@Override
	public void update(Hotel hotel) {
		// TODO Auto-generated method stub
		this.getHibernateTemplate().update(hotel);
	}

	@Override
	public void delete(Hotel hotel) {
		// TODO Auto-generated method stub
		int p1, p2, p3, p4;
		String hql1 = "select r from Route r where r.point1 = '"+hotel.getName()+"'";
		List<Route> list1 = this.getHibernateTemplate().find(hql1);
		p1 = list1.size();
		String hql2 = "select r from Route r where r.point2 = '"+hotel.getName()+"'";
		List<Route> list2 = this.getHibernateTemplate().find(hql2);
		p2 = list2.size();
		String hql3 = "select r from Route r where r.point3 = '"+hotel.getName()+"'";
		List<Route> list3 = this.getHibernateTemplate().find(hql3);
		p3 = list3.size();
		String hql4 = "select r from Route r where r.point4 = '"+hotel.getName()+"'";
		List<Route> list4 = this.getHibernateTemplate().find(hql4);
		p4 = list4.size();
		if(p1!=0){
			for(int i=0;i<p1;i++){
				route = (Route)this.getHibernateTemplate().find(hql1).get(i);
				route.setPoint1("");
				route.setDuration1("");
				this.getHibernateTemplate().update(route);
			}
			
		}
		if(p2!=0){
			for(int i=0;i<p2;i++){
				route = (Route)this.getHibernateTemplate().find(hql2).get(0);
				route.setPoint2("");
				route.setDuration2("");
				this.getHibernateTemplate().update(route);
			}
			
		}
		if(p3!=0){
			for(int i=0;i<p3;i++){
				route = (Route)this.getHibernateTemplate().find(hql3).get(0);
				route.setPoint3("");
				route.setDuration3("");
				this.getHibernateTemplate().update(route);
			}
			
		}
		if(p4!=0){
			for(int i=0;i<p4;i++){
				route = (Route)this.getHibernateTemplate().find(hql4).get(0);
				route.setPoint4("");
				route.setDuration4("");
				this.getHibernateTemplate().update(route);
			}
			
		}
		String hql5 = "select h from Hotel h where h.name='"+hotel.getName()+"'";
		hotel = (Hotel)this.getHibernateTemplate().find(hql5).get(0);
		this.getHibernateTemplate().delete(hotel);
	}

	@Override
	public List<Hotel> listName(Hotel hotel) {
		// TODO Auto-generated method stub
		String hql = "select h from Hotel h where h.name = '"+hotel.getName()+"'";
		return (List<Hotel>)this.getHibernateTemplate().find(hql);
	}

}
