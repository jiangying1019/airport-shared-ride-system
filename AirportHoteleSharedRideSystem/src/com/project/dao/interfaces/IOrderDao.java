package com.project.dao.interfaces;

import java.util.List;

import com.project.entities.Order;

public interface IOrderDao {
	boolean add(Order order);
	List<Order> list(Order order);
	int update(Order order);//revise order  0 means the order doesn't belong to the user. 1 means the order belongs to the user.
	Order detail(Order order);
	List<Order> checkOrders(Order order);
}
