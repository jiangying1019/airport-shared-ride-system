package com.project.dao.interfaces;

import java.util.List;

import com.project.entities.Route;

public interface IRouteDao {
	void add(Route route);
	void delete(Route route);
	Route detail(Route route);
	List<Route> list(Route route);
	List<Route> listName(Route route);
	int checkPoint(Route route);
	int checkRouteExists(Route route);
	List<Route> searchAvailableBus(Route route);
}
