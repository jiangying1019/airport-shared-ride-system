package com.project.dao.interfaces;

import java.util.List;

import com.project.entities.Bus;

public interface IBusDao {
	void add(Bus bus);
	void delete(Bus bus);
	List<Bus> list(Bus bus);
	boolean changeRoute(Bus bus);
	List<Bus> listName(Bus bus);
	List<Bus> listBusesNeedToBeChanged(Bus bus);
	void deleteRouteThenChangeRoute(Bus bus);
}
