package com.project.dao.interfaces;

import java.util.List;

import com.project.entities.SecurityQuestion;

public interface ISecurityQuestionDao {
		void add(SecurityQuestion securityQuestion);
		List<SecurityQuestion> list(SecurityQuestion securityQuestion);
		SecurityQuestion detail(SecurityQuestion securityQuestion);
		void delete(SecurityQuestion securityQuestion);
}
