package com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.dao.interfaces.ISecurityQuestionDao;
import com.project.entities.SecurityQuestion;
import com.project.service.interfaces.ISecurityQuestionService;

@Service("securityQuestionService")
public class SecurityQuestionService implements ISecurityQuestionService{
	
	@Resource(name="securityQuestionDao")
	private ISecurityQuestionDao securityQuestionDao;

	@Override
	public void add(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
		securityQuestionDao.add(securityQuestion);
	}

	@Override
	public List<SecurityQuestion> list(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
		return securityQuestionDao.list(securityQuestion);
	}

	@Override
	public SecurityQuestion detail(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
		return securityQuestionDao.detail(securityQuestion);
	}

	@Override
	public void delete(SecurityQuestion securityQuestion) {
		// TODO Auto-generated method stub
	securityQuestionDao.delete(securityQuestion);	
	}
}
