package com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.dao.interfaces.IBusDao;
import com.project.entities.Bus;
import com.project.service.interfaces.IBusService;

@Service("busService")
public class BusService implements IBusService{

	@Resource(name="busDao")
	private IBusDao busDao;
	
	@Override
	public void add(Bus bus) {
		// TODO Auto-generated method stub
	busDao.add(bus);
	}

	@Override
	public void delete(Bus bus) {
		// TODO Auto-generated method stub
	busDao.delete(bus);
	}

	@Override
	public List<Bus> list(Bus bus) {
		// TODO Auto-generated method stub
		return busDao.list(bus);
	}

	@Override
	public boolean changeRoute(Bus bus) {
		// TODO Auto-generated method stub
		return busDao.changeRoute(bus);
		
	}

	@Override
	public List<Bus> listName(Bus bus) {
		// TODO Auto-generated method stub
		return busDao.listName(bus);
	}

	@Override
	public List<Bus> listBusesNeedToBeChanged(Bus bus) {
		// TODO Auto-generated method stub
		return busDao.listBusesNeedToBeChanged(bus);
	}

	@Override
	public void deleteRouteThenChangeRoute(Bus bus) {
		// TODO Auto-generated method stub
		busDao.deleteRouteThenChangeRoute(bus);
	}
	
}
