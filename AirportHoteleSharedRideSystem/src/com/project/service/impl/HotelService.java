package com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.dao.interfaces.IHotelDao;
import com.project.entities.Hotel;
import com.project.service.interfaces.IHotelService;

@Service("hotelService")
public class HotelService implements IHotelService{

	@Resource(name="hotelDao")
	private IHotelDao hotelDao;
	
	@Override
	public void add(Hotel hotel) {
		// TODO Auto-generated method stub
		hotelDao.add(hotel);
	}

	@Override
	public List<Hotel> list(Hotel hotel) {
		// TODO Auto-generated method stub
		return hotelDao.list(hotel);
	}

	@Override
	public void update(Hotel hotel) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(Hotel hotel) {
		// TODO Auto-generated method stub
	hotelDao.delete(hotel);	
	}

	@Override
	public List<Hotel> listName(Hotel hotel) {
		// TODO Auto-generated method stub
		return hotelDao.listName(hotel);
	}

}
