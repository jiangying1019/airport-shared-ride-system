package com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.dao.interfaces.IRouteDao;
import com.project.entities.Bus;
import com.project.entities.Route;
import com.project.service.interfaces.IRouteService;

@Service("routeService")
public class RouteService implements IRouteService {

	@Resource(name="routeDao")
	private IRouteDao routeDao;
	
	@Override
	public void add(Route route) {
		// TODO Auto-generated method stub
	routeDao.add(route);	
	}

	@Override
	public void delete(Route route) {
		// TODO Auto-generated method stub
		routeDao.delete(route);
	}

	@Override
	public Route detail(Route route) {
		// TODO Auto-generated method stub
		return routeDao.detail(route);
	}

	@Override
	public List<Route> list(Route route) {
		// TODO Auto-generated method stub
		return routeDao.list(route);
	}

	@Override
	public List<Route> listName(Route route) {
		// TODO Auto-generated method stub
		return routeDao.listName(route);
	}

	@Override
	public int checkPoint(Route route) {
		// TODO Auto-generated method stub
		return routeDao.checkPoint(route);
	}

	@Override
	public int checkRouteExists(Route route) {
		// TODO Auto-generated method stub
		return routeDao.checkRouteExists(route);
	}

	@Override
	public List<Route> searchAvailableBus(Route route) {
		// TODO Auto-generated method stub
		return routeDao.searchAvailableBus(route);
	}

}
