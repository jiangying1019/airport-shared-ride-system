package com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.dao.impl.UserDao;
import com.project.dao.interfaces.IAdminDao;
import com.project.entities.Admin;
import com.project.service.interfaces.IAdminService;

@Service("adminService")
public class AdminService implements IAdminService{
	
	@Resource(name="adminDao")
	private IAdminDao adminDao;
	
	@Override
	public List<Admin> list(Admin admin) {
		// TODO Auto-generated method stub
		return adminDao.list(admin);
	}

	@Override
	public void update(Admin admin) {
		// TODO Auto-generated method stub
		adminDao.update(admin);
	}

	@Override
	public Admin detail(Admin admin) {
		// TODO Auto-generated method stub
		return adminDao.detail(admin);
	}


	@Override
	public Admin checkin(Admin admin) {
		// TODO Auto-generated method stub
		return adminDao.checkin(admin);
	}
	
}
