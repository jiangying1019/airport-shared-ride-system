package com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.dao.interfaces.IOrderDao;
import com.project.entities.Order;
import com.project.service.interfaces.IOrderService;

@Service("orderService")
public class OrderService implements IOrderService{

	@Resource(name="orderDao")
	private IOrderDao orderDao;
	
	@Override
	public boolean add(Order order) {
		// TODO Auto-generated method stub
		return orderDao.add(order);
	}

	@Override
	public List<Order> list(Order order) {
		// TODO Auto-generated method stub
		return orderDao.list(order);
	}

	@Override
	public int update(Order order) {
		// TODO Auto-generated method stub
		if(orderDao.update(order)==0)
			return 0;
		else return 1;
	}

	@Override
	public Order detail(Order order) {
		// TODO Auto-generated method stub
		return orderDao.detail(order);
	}

	@Override
	public List<Order> checkOrders(Order order) {
		// TODO Auto-generated method stub
		
		return orderDao.checkOrders(order);
	}

}
