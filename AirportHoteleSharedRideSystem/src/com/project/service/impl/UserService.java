package com.project.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.dao.interfaces.IUserDao;
import com.project.entities.User;
import com.project.service.interfaces.IUserService;

@Service("userService")
public class UserService implements IUserService{
	
	@Resource(name="userDao")
	private IUserDao userDao;
	
	@Override
	public void add(User user) {
		// TODO Auto-generated method stub
		 userDao.add(user);
	}

	@Override
	public List<User> list(User user) {
		// TODO Auto-generated method stub
		return userDao.list(user);
	}

	@Override
	public void update(User user) {
		// TODO Auto-generated method stub
		 userDao.update(user);
	}
	
	@Override
	public User detail(User user) {
		// TODO Auto-generated method stub
		return userDao.detail(user);
	}

	@Override
	public void delete(User user) {
		// TODO Auto-generated method stub
		userDao.delete(user);
	}

	@Override
	public User checkin(User user) {
		// TODO Auto-generated method stub
		return userDao.checkin(user);
	}

	@Override
	public boolean checkEmailIsUnique(User user) {
		// TODO Auto-generated method stub
		return userDao.checkEmailIsUnique(user);
	}

	@Override
	public User searchForSecurityQuestion(User user) {
		// TODO Auto-generated method stub
		return userDao.searchForSecurityQuestion(user);
	}
}
