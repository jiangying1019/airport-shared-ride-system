package com.project.service.interfaces;

import java.util.List;

import com.project.entities.Admin;

public interface IAdminService {

	List<Admin> list(Admin admin);
	void update(Admin admin);
	Admin detail(Admin admin);
	Admin checkin(Admin admin);
}
