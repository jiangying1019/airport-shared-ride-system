package com.project.service.interfaces;

import java.util.List;

import com.project.entities.User;

public interface IUserService {

	void add(User user);
	List<User> list(User user);
	void update(User user);
	User detail(User user);
	void delete(User user);
	User checkin(User user);
	boolean checkEmailIsUnique(User user);
	User searchForSecurityQuestion(User user);
}
