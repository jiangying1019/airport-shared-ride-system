package com.project.service.interfaces;

import java.util.List;

import com.project.entities.Order;

public interface IOrderService {
	boolean add(Order order);
	List<Order> list(Order order);
	int update(Order order);//revise order
	Order detail(Order order);
	List<Order> checkOrders(Order order);
}
