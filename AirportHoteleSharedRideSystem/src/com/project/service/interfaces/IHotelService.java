package com.project.service.interfaces;

import java.util.List;

import com.project.entities.Hotel;

public interface IHotelService {
	void add(Hotel hotel);
	List<Hotel> list(Hotel hotel);
	void update(Hotel hotel);
	void delete(Hotel hotel);
	List<Hotel> listName(Hotel hotel);
}
