package com.project.service.interfaces;

import java.util.List;

import com.project.entities.SecurityQuestion;

public interface ISecurityQuestionService {
	void add(SecurityQuestion securityQuestion);
	List<SecurityQuestion> list(SecurityQuestion securityQuestion);
	SecurityQuestion detail(SecurityQuestion securityQuestion);
	void delete(SecurityQuestion securityQuestion);
}
