package com.project.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.project.entities.SecurityQuestion;
import com.project.service.interfaces.ISecurityQuestionService;

@SuppressWarnings("serial")
@Controller
@Scope("prototype")
@org.apache.struts2.config.ParentPackage(value="struts-default")
@Results({ @Result(name = "successAddSecurityQuestion", value = "successAddSecurityQuestion.jsp")
	 })
public class SecurityQuestionAction extends ActionSupport {
	@Resource(name="securityQuestionService")
	private ISecurityQuestionService securityQuestionService;
	private List<SecurityQuestion> list;
	private SecurityQuestion securityQuestion;
	public ISecurityQuestionService getSecurityQuestionService() {
		return securityQuestionService;
	}
	public void setSecurityQuestionService(ISecurityQuestionService securityQuestionService) {
		this.securityQuestionService = securityQuestionService;
	}
	public List<SecurityQuestion> getList() {
		return list;
	}
	public void setList(List<SecurityQuestion> list) {
		this.list = list;
	}
	public SecurityQuestion getSecurityQuestion() {
		return securityQuestion;
	}
	public void setSecurityQuestion(SecurityQuestion securityQuestion) {
		this.securityQuestion = securityQuestion;
	}
	
	public String add()throws Exception{
		securityQuestionService.add(securityQuestion);
		return "successAddSecurityQuestion";
	}
	
	public String list()throws Exception{
		list = securityQuestionService.list(securityQuestion);
		return "list";
	}
	
	public String delete()throws Exception{
		securityQuestionService.delete(securityQuestion);
		return "successDeleteSecurityQuestion";
	}
}
