package com.project.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.project.entities.Route;
import com.project.service.interfaces.IRouteService;

@SuppressWarnings("serial")
@Controller
@Scope("prototype")
@org.apache.struts2.config.ParentPackage(value="struts-default")
@Results({ @Result(name = "successAddRoute", value = "successAddRoute.jsp"),
	@Result(name = "successDeleteRouteReassignBus", value = "successDeleteRouteReassignBus.jsp"),
	@Result(name = "routeNameNotUnique", value = "routeNameNotUnique.jsp"),
	@Result(name = "pointSetIsDepulicated", value = "pointSetIsDepulicated.jsp"),
	@Result(name = "list", value = "listRoutes.jsp"),
	@Result(name = "noAvailableRoute", value = "noAvailableRoute.jsp"),
	@Result(name = "haveAvailableRoute", value = "haveAvailableRoute.jsp"),
	@Result(name = "noSuchRoute", value = "noSuchRoute.jsp")})
public class RouteAction {
	@Resource(name="routeService")
	private IRouteService routeService;
	private List<Route> list;
	private Route route;
	public IRouteService getRouteService() {
		return routeService;
	}
	public void setRouteService(IRouteService routeService) {
		this.routeService = routeService;
	}
	public List<Route> getList() {
		return list;
	}
	public void setList(List<Route> list) {
		this.list = list;
	}
	public Route getRoute() {
		return route;
	}
	public void setRoute(Route route) {
		this.route = route;
	}
	
	public String add()throws Exception{
		if(routeService.listName(route).size()>0){
			return "routeNameNotUnique";
		}
		if(routeService.checkPoint(route)==0){
			return "pointSetIsDepulicated";
		}
		else{
			routeService.add(route);
			return "successAddRoute";
		}
		
	}
	
	public String delete()throws Exception{
			routeService.delete(route);
			return "successDeleteRouteReassignBus";
	}
	
	public String detail()throws Exception{
		routeService.detail(route);
		return "detail";
	}
	
	public String list()throws Exception{
		list = routeService.list(route);
		return "list";
	}
	
	public String searchAvailableBus()throws Exception{
		list = routeService.searchAvailableBus(route);
		if(list.size()==0){
			return "noAvailableRoute";
		}else{
			return "haveAvailableRoute";
		}
		
	}
}
