package com.project.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.project.entities.Hotel;
import com.project.service.interfaces.IHotelService;

@SuppressWarnings("serial")
@Controller
@Scope("prototype")
@org.apache.struts2.config.ParentPackage(value="struts-default")
@Results({ @Result(name = "hotelNameIsNotUnique", value = "hotelNameIsNotUnique.jsp"),
	@Result(name = "successAddHotel", value = "successAddHotel.jsp"),
	@Result(name = "noSuchHotel", value = "noSuchHotel.jsp"),
	@Result(name = "successDeleteHotel", value = "successDeleteHotel.jsp") })

public class HotelAction extends ActionSupport{
	@Resource(name="hotelService")
	private IHotelService hotelService;
	private List<Hotel> list;
	private Hotel hotel;
	public IHotelService getHotelService() {
		return hotelService;
	}
	public void setHotelService(IHotelService hotelService) {
		this.hotelService = hotelService;
	}
	public List<Hotel> getList() {
		return list;
	}
	public void setList(List<Hotel> list) {
		this.list = list;
	}
	public Hotel getHotel() {
		return hotel;
	}
	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}
	
	public String add()throws Exception{
		if(hotelService.listName(hotel).size()>0){
			return "hotelNameIsNotUnique";
		}
		else{
			hotelService.add(hotel);
			return "successAddHotel";
		}
	}
	
	public String delete()throws Exception{
		if(hotelService.listName(hotel).size()==0){
			return "noSuchHotel";
		}
		else{
			hotelService.delete(hotel);
			return "successDeleteHotel";
		}
		
	}
	
	public String list()throws Exception{
		list = hotelService.list(hotel);
		return "list";
	}
	
	public String update()throws Exception{
		hotelService.update(hotel);
		return "update";
	}

}
