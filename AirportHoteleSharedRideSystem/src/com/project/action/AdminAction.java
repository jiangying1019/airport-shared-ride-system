package com.project.action;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;

import com.project.entities.Admin;
import com.project.entities.User;
import com.project.service.interfaces.IAdminService;
import com.opensymphony.xwork2.ActionSupport;

@SuppressWarnings("serial")
@Controller
@Scope("prototype")
@org.apache.struts2.config.ParentPackage(value="struts-default")
@Results({ @Result(name = "success", value = "adminsLoginSuccess.jsp"),
	@Result(name = "failure", value = "admins.jsp") })

public class AdminAction extends ActionSupport{
	
	@Resource(name="adminService")
	private IAdminService adminService;
	private List<Admin> list;
	private Admin admin;
	
	public List<Admin> getList() {
		return list;
	}
	public void setList(List<Admin> list) {
		this.list = list;
	}
	
	
	public Admin getAdmin() {
		return admin;
	}
	
	public void setAdmin(Admin admin) {
		this.admin = admin;
	}
	
	public String update() throws Exception{
		adminService.update(admin);
		return "success";
	}
	
	public String list()throws Exception{
		list = adminService.list(admin);
		return "list";
	}
	
	
	public String detail() throws Exception{
		adminService.detail(admin);
		return "detail";
	}
	
	
	public String checkin() throws Exception{
		admin = adminService.checkin(admin);
		if(!admin.getAid().isEmpty())
			return "success";
		return "failure";
	}
	
	
	public String jumpToManageBookPage()throws Exception{
		return "success";
	}
}
