package com.project.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionSupport;
import com.project.entities.Bus;
import com.project.service.interfaces.IBusService;
import com.project.service.interfaces.IRouteService;

@SuppressWarnings("serial")
@Controller
@Scope("prototype")
@org.apache.struts2.config.ParentPackage(value="struts-default")
@Results({ @Result(name = "busNameIsNotUnique", value = "busNameIsNotUnique.jsp"),
	@Result(name = "addSuccess", value = "addBusSuccess.jsp"),
	@Result(name = "noSuchEntity", value = "noSuchBusEntityToDelete.jsp"),
	@Result(name = "deleteSuccess", value = "deleteBusSuccess.jsp"),
	@Result(name = "noSuchEntityToChangeRoute", value = "noSuchBusEntityToChangeRoute.jsp"),
	@Result(name = "listBusesNeedToBeChanged", value = "listBusesNeedToBeChanged.jsp"),
	@Result(name = "deleteRouteThenChangeRoute", value = "deleteRouteThenChangeRoute.jsp"), 
	@Result(name = "list", value = "listBuses.jsp"),
	@Result(name = "reassignToTheSameRoute", value = "reassignToTheSameRoute.jsp"),
	@Result(name = "noSuchRouteForBusToChange", value = "noSuchRouteForBusToChange.jsp"),
	@Result(name = "changeRouteSuccess", value = "changeRouteSuccess.jsp")})

public class BusAction extends ActionSupport{
	@Resource(name="busService")
	private IBusService busService;
	private IRouteService routeService;
	private List<Bus> list;
	private Bus bus;
	
	public IRouteService getRouteService() {
		return routeService;
	}
	public void setRouteService(IRouteService routeService) {
		this.routeService = routeService;
	}

	public IBusService getBusService() {
		return busService;
	}
	public void setBusService(IBusService busService) {
		this.busService = busService;
	}
	public List<Bus> getList() {
		return list;
	}
	public void setList(List<Bus> list) {
		this.list = list;
	}
	public Bus getBus() {
		return bus;
	}
	public void setBus(Bus bus) {
		this.bus = bus;
	}
	
	public String add()throws Exception{
		if(busService.listName(bus).size()>0){
			return "busNameIsNotUnique";
		}
		else{
			busService.add(bus);
			return "addSuccess";
		}
		
	}
	
	public String delete()throws Exception{
		
		if(busService.listName(bus).size()==0){
			return "noSuchEntity";
		}
		else{
			busService.delete(bus);
			return "deleteSuccess";
		}
	}
	
	public String list() throws Exception{
		list = busService.list(bus);
		return "list";
	}
	
	public String changeRoute()throws Exception{
		
		if(busService.changeRoute(bus))
			return "changeRouteSuccess";
		return "reassignToTheSameRoute";
		
	}
	
	public String listBusesNeedToBeChanged()throws Exception{
		list = busService.listBusesNeedToBeChanged(bus);
		return "listBusesNeedToBeChanged";
	}
	
	public String deleteRouteThenChangeRoute()throws Exception{
		busService.deleteRouteThenChangeRoute(bus);
		return "deleteRouteThenChangeRoute";
	}
}
