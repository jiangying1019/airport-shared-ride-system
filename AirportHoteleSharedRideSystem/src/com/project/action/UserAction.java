package com.project.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.project.entities.User;
import com.project.service.interfaces.IUserService;

@SuppressWarnings("serial")
@Controller
@Scope("prototype")
@org.apache.struts2.config.ParentPackage(value="struts-default")
@Results({ @Result(name = "add", value = "adduser.jsp"),
	@Result(name = "login", value = "emailLogin.jsp"),
	@Result(name = "success", value = "loginSuccess.jsp"),
	@Result(name = "failure", value = "loginFailed.jsp"),   
	@Result(name = "emailIsNotUnique", value = "emailIsNotUnique.jsp"),
	@Result(name = "registerSuccess", value = "registerSuccess.jsp"),
	@Result(name = "noSuchUser", value = "noSuchUser.jsp"),
	@Result(name = "delete", value = "delete.jsp")})
public class UserAction extends ActionSupport{

	@Resource(name="userService")
	private IUserService userService;
	private List<User> list;
	private User user;
	
	public List<User> getList(){
		return list;
	}
	
	public void setList(List<User> list){
		this.list = list;
	}
	
	public User getUser(){
		return user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
	
	public String add() throws Exception{
		if(userService.checkEmailIsUnique(user)){
			userService.add(user);
			return "registerSuccess";
		}
		return "emailIsNotUnique";
		
	}
	
	public String update() throws Exception{
		userService.update(user);
		return "success";
	}
	
	public String list() throws Exception{
		list = userService.list(user);
		return "list";
	}
	
	public String delete() throws Exception{
		userService.delete(user);
		return "delete";
	}
	
	public String detail() throws Exception{
		userService.detail(user);
		return "detail";
	}
	
	public String gotoAdd() throws Exception{
		return "add";
	}
	
	public String login()throws Exception{
		return "login";
	}
	
	public String checkin()throws Exception{
		user = userService.checkin(user);
		if(!user.getUid().isEmpty()){
			ActionContext.getContext().getApplication().put("user", user);
			return "success";
		}else{
			return "failure";
		}
	}
	
	public String searchForSecurityQuestion()throws Exception{
		if(userService.searchForSecurityQuestion(user)==null){
			return "noSuchUser";
		}else{
			return "login";
		}
	}
}
