package com.project.action;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.config.Result;
import org.apache.struts2.config.Results;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;
import com.project.entities.Order;
import com.project.entities.User;
import com.project.service.interfaces.IOrderService;



@SuppressWarnings("serial")
@Controller
@Scope("prototype")
@org.apache.struts2.config.ParentPackage(value="struts-default")
@Results({ 
	@Result(name = "successGeneratedAnOrder", value = "successAddAnOrder.jsp"),
	@Result(name = "list", value = "orders.jsp"),
	@Result(name = "checkOrders", value = "orders.jsp"),
	@Result(name = "reviseOrderCheckBusAvailable", value = "reviseOrderCheckBusAvailable.jsp"),
	@Result(name = "noSuchBusCoversApproachAndDestination", value = "noSuchBusCoversApproachAndDestination.jsp"),
	@Result(name = "update", value = "successReviseAnOrder.jsp")})
public class OrderAction extends ActionSupport{
	
	@Resource(name="orderService")
	private IOrderService orderService;
	private List<Order> list;
	private Order order;
	private User user;
	
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public IOrderService getOrderService() {
		return orderService;
	}
	public void setOrderService(IOrderService orderService) {
		this.orderService = orderService;
	}
	public List<Order> getList() {
		return list;
	}
	public void setList(List<Order> list) {
		this.list = list;
	}
	public Order getOrder() {
		return order;
	}
	public void setOrder(Order order) {
		this.order = order;
	}
	
	public String update() throws Exception{
		if(orderService.update(order)==0)
			return "reviseOrderCheckBusAvailable";
		else {
			user = (User)ActionContext.getContext().getApplication().get("user");
			return "update";
		}
			
	}
	
	public String add() throws Exception{
		user = (User)ActionContext.getContext().getApplication().get("user");
		if(orderService.add(order)){
			return "successGeneratedAnOrder";
		}
		return "noSuchBusCoversApproachAndDestination";
	}
	
	
	public String detail()throws Exception{
		user = (User)ActionContext.getContext().getSession().get("user");
		orderService.detail(order);
		return "detail";
	}
	
	public String list()throws Exception{
		user = (User)ActionContext.getContext().getSession().get("user");
		list = orderService.list(order);
		return "list";
	}
	
	public String checkOrders()throws Exception{
		user = (User)ActionContext.getContext().getApplication().get("user");
		list = orderService.checkOrders(order);
		return "checkOrders";
	}
}
