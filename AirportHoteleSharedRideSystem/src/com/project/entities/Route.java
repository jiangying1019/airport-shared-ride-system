package com.project.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="route")
public class Route {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="routeid")
	private int routeid;
	
	@Column(name="name")
	private String name;
	
	@Column(name="point1")
	private String point1;
	
	@Column(name="point2")
	private String point2;
	
	@Column(name="point3")
	private String point3;
	
	@Column(name="point4")
	private String point4;

	@Column(name="duration1")
	private String duration1;
	
	
	@Column(name="duration2")
	private String duration2;
	
	@Column(name="duration3")
	private String duration3;
	
	@Column(name="duration4")
	private String duration4;

	public int getRouteid() {
		return routeid;
	}

	public String getPoint1() {
		return point1;
	}

	public void setPoint1(String point1) {
		this.point1 = point1;
	}

	public String getPoint2() {
		return point2;
	}

	public void setPoint2(String point2) {
		this.point2 = point2;
	}

	public String getPoint3() {
		return point3;
	}

	public void setPoint3(String point3) {
		this.point3 = point3;
	}

	public String getPoint4() {
		return point4;
	}

	public void setPoint4(String point4) {
		this.point4 = point4;
	}

	public String getDuration1() {
		return duration1;
	}

	public void setDuration1(String duration1) {
		this.duration1 = duration1;
	}

	public String getDuration2() {
		return duration2;
	}

	public void setDuration2(String duration2) {
		this.duration2 = duration2;
	}

	public String getDuration3() {
		return duration3;
	}

	public void setDuration3(String duration3) {
		this.duration3 = duration3;
	}

	public String getDuration4() {
		return duration4;
	}

	public void setDuration4(String duration4) {
		this.duration4 = duration4;
	}

	public void setRouteid(int routeid) {
		this.routeid = routeid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
